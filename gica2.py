#!/usr/bin/env python
# coding: utf-8

# In[1]:


#Import libraries necessary to do the analysis

import km3io as kio
import km3db
import km3pipe as kp
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.pyplot import figure


# In[2]:


#print current directory


# In[3]:


pwd


# In[4]:


#define fileName, either simulation or data
fileName = '/home/stefanescu/internship/muonstopwork/rootfiles/data_dimitrisSelection.root'


# In[5]:


#Read file using kio
f = kio.OfflineReader(fileName)


# In[6]:


#print all parameters for an event
for evt in f.events:
    print (dir(evt))  #list of parameter/attributes per event
    break


# In[7]:


#list of parameters per event in fields variable
evt._fields


# In[8]:


#list of parameters and actual value for an event
i=0
for name in evt:
    print(evt._fields[i], name)
    i+=1


# In[9]:


# t_sec + t_ns + t_hit  !!!Not working t_hit
np.datetime64(evt.t_sec,'s') + np.timedelta64(evt.t_ns , 'ns') #+ np.timedelta64(int(evt.hits.t[0]) , 'ns')


# In[10]:


#Hits variable empty!!!
for var in evt.hits:
    print(var)
print(evt.hits)


# In[11]:


#print info of each hit, BUT EMPTY!!!!!
for i in range(evt.n_hits):
    print(evt.hits.t[i], evt.hits.tot[i], evt.hits.trig[i], evt.hits.dom_id[i])


# In[12]:


#Tracks variable !!!
for var in evt.tracks:
    print(var)
print(evt.tracks)


# In[13]:


#print info for each track
print("lik              rec_stages           z direction")
for i in range(evt.n_tracks):
    print (evt.tracks.lik[i], evt.tracks.rec_stages[i], evt.tracks.dir_z[i])


# In[14]:


#Some useful prints


# In[15]:


print(evt.tracks.E)


# In[16]:


#total number of events
len(f.events)


# In[17]:


#array of booleans, true if n_tracks >= 1
haveTrk = np.array(f.events.n_tracks, dtype=bool)

#filter events based on E > 2 GeV qnd lik > 200 
cuts = (f.events.tracks.E[haveTrk][:,0] > 2.) & (f.events.tracks.lik[haveTrk][:,0] > 200.) 

#print z directions of tracks
print (f.events.tracks.dir_z[haveTrk][:,0])


#histogram of evnts for z direction (-1 corresponds to vertical downwarding direction)
#line correponds to cut events

fig, axe = plt.subplots()

h = axe.hist(f.events.tracks.dir_z[haveTrk][:,0],
             bins = np.linspace(-1,1,101), histtype='stepfilled')
h = axe.hist(f.events.tracks.dir_z[haveTrk][cuts][:,0],
             bins = np.linspace(-1,1,101), histtype='step')
axe.set_yscale('log')


# In[18]:


#2d histogram, E vs z direction
fig, axe = plt.subplots()
axe.set_xscale('log')
h2 = axe.hist2d(f.events.tracks.E[haveTrk][:,0].__array__(), f.events.tracks.dir_z[haveTrk][:,0].__array__(),
           bins = [10**np.linspace(-1,3,41), np.linspace(-1,1,101)], norm=mpl.colors.LogNorm())
axe.set_xlabel('GeV')
fig.colorbar(h2[3])

#2d histogram, lik vs z direction
fig, axe = plt.subplots()
h2 = axe.hist2d(f.events.tracks.lik[haveTrk][:,0].__array__(), 
                f.events.tracks.dir_z[haveTrk][:,0].__array__(),
                bins = [np.linspace(-100,1000,111), np.linspace(-1,1,101)], norm=mpl.colors.LogNorm())
fig.colorbar(h2[3])


# In[19]:


#2d histogram, E vs z direction for cut evetns
fig, axe = plt.subplots()
axe.set_xscale('log')
h2 = axe.hist2d(f.events.tracks.E[haveTrk][cuts][:,0].__array__(), f.events.tracks.dir_z[haveTrk][cuts][:,0].__array__(),
           bins = [10**np.linspace(-1,3,41), np.linspace(-1,1,101)], norm=mpl.colors.LogNorm())
axe.set_xlabel('GeV')
fig.colorbar(h2[3])

#2d histogram, lik vs z direction for cut events
fig, axe = plt.subplots()
h2 = axe.hist2d(f.events.tracks.lik[haveTrk][cuts][:,0].__array__(), 
                f.events.tracks.dir_z[haveTrk][cuts][:,0].__array__(),
                bins = [np.linspace(-100,1000,111), np.linspace(-1,1,101)], norm=mpl.colors.LogNorm())
fig.colorbar(h2[3])


# In[20]:


#2d histogram, E vs lik direction for all track events and for cutted events

fig, axe = plt.subplots()
axe.set_xscale('log')
h2 = axe.hist2d(f.events.tracks.E[haveTrk][:,0].__array__(), f.events.tracks.lik[haveTrk][:,0].__array__(),
           bins = [10**np.linspace(-1,3,41), np.linspace(-100,1000,111)], norm=mpl.colors.LogNorm())
axe.set_xlabel('GeV')
axe.set_ylabel('lik')
fig.colorbar(h2[3])
axe.set_title("Energy vs lik")


fig, axe = plt.subplots()
axe.set_xscale('log')
h2 = axe.hist2d(f.events.tracks.E[haveTrk][cuts][:,0].__array__(), f.events.tracks.lik[haveTrk][cuts][:,0].__array__(),
           bins = [10**np.linspace(-1,3,41), np.linspace(-100,1000,111)], norm=mpl.colors.LogNorm())
axe.set_xlabel('GeV')
axe.set_ylabel('lik')
axe.set_title("Energy vs lik (cuts)")
fig.colorbar(h2[3])


# In[46]:


#All events have the same rec_stages variable


# In[49]:


n = []
for i in f.events.tracks.rec_stages:
    n.append(len(i[0,:]))
    print(len(i[0,:]))    


# In[53]:


#1D histogram rec_stage frequencies  Only 10% of data shown
fig, axe = plt.subplots()
h = axe.hist(n,bins=(0,6,7))
axe.set_yscale("log")
plt.title("rec_stage frequencies")


# In[57]:


#2d histogram, rec_stages vs lik for all track events
fig, axe = plt.subplots()
h2 = axe.hist2d(f.events.tracks.lik[haveTrk][:5548176,0].__array__(), n,
           bins = [np.linspace(-100,1000,111), (0,6,7)], norm=mpl.colors.LogNorm())
axe.set_xlabel('lik')
axe.set_ylabel('rec_stage module')
fig.colorbar(h2[3])


# In[59]:


#longitude of n list, too slow to get it all, corresponds just to 10%
print(len(n))
print(len(n)/len(f.events)*100)


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[60]:


#Array of initial position x, y and z coordinates for each event considering the cut
pos_start = [f.events.tracks.pos_x[haveTrk][cuts][:,0].__array__(),
             f.events.tracks.pos_y[haveTrk][cuts][:,0].__array__(),
             f.events.tracks.pos_z[haveTrk][cuts][:,0].__array__()] 

#Add all positions together, array of arrays
pos_start = np.stack(pos_start)


# In[61]:


#Plots of starting position for x vs z, y vs z


figure(figsize=(20, 20))
fig, (ax1, ax2) = plt.subplots(1, 2)
fig.suptitle('Starting position of track', fontsize=14, y=1.12)
ax1.plot(pos_start[0],pos_start[2],".",markersize=0.1)
ax1.set_xlabel('X (m)')
ax1.set_ylabel('Z (m)')

ax2.plot(pos_start[0],pos_start[1],".",markersize=0.1)
ax2.set_xlabel('X (m)')
ax2.set_ylabel('Y (m)')

fig.tight_layout()

#More statring points in the top of the detector (high Z)
#Uniform distribution in the x-y plane


# In[ ]:


#Fitinf for each track of each event. Get all fitinf of 0th and 10th element of fitinf object (Trak lenght)
trackLength = f.events.tracks.fitinf[haveTrk][cuts][:,0,10].__array__()


# In[ ]:


#stop postion, as pos start + track
pos_stop_x  = pos_start[0,:] + f.events.tracks.dir_x[haveTrk][cuts][:,0].__array__()*trackLength # for x
pos_stop_y  = pos_start[1,:] + f.events.tracks.dir_y[haveTrk][cuts][:,0].__array__()*trackLength # for y
pos_stop_z  = pos_start[2,:] + f.events.tracks.dir_z[haveTrk][cuts][:,0].__array__()*trackLength # for z

pos_stop = np.stack((pos_stop_x, pos_stop_y, pos_stop_z))


# In[ ]:


#2d histogram, initial z vs initial x
fig, axes = plt.subplots()
axes[0].set_aspect('equal')
h2 = axes[0].hist2d(pos_start[0], pos_start[2], bins= [np.linspace(350,600,251), np.linspace(0,250, 251)], norm=mpl.colors.LogNorm())


# In[ ]:


#2d histogram, stop z vs stop x
fig, axes = plt.subplots()
axes[0].set_aspect('equal')
h2 = axes[0].hist2d(pos_stop[0], pos_stop[2], bins= [np.linspace(350,600,251), np.linspace(0,250, 251)], norm=mpl.colors.LogNorm())


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:


#Read DOMS information on positions and id
det = kp.hardware.Detector('/home/stefanescu/internship/muonstopwork/KM3NeT_00000049_20200625.detx')
print(det)


# In[ ]:


#Store position of DOMS in a np.array ([xxx][yyyy][zzzz])
dom_x = []
dom_y = []
dom_z = []

for dom,pos in det.dom_positions.items():
    dom_x.append(pos[0])
    dom_y.append(pos[1])
    dom_z.append(pos[2])
    
dom_positions = np.array((dom_x,dom_y,dom_z))


# In[ ]:


fig, axes = plt.subplots(1,2, figsize=[10,10])
axes[0].set_aspect('equal')
#2d histogram, final z vs initial x
h2 = axes[0].hist2d(pos_stop[0], pos_stop[2], bins= [np.linspace(350,600,251), np.linspace(0,250, 251)], norm=mpl.colors.LogNorm())


# In[ ]:





# In[ ]:


#3D plot stop positions

from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure()
ax = Axes3D(fig)

x = pos_stop[0]
y = pos_stop[1]
z = pos_stop[2]

ax.scatter(x,y,z,marker=".",markersize=0.1,label="first")

ax1.scatter(x[:4], y[:4], s=10, c='b', marker="s", label='first')
ax1.scatter(x[40:],y[40:], s=10, c='r', marker="o", label='second')
plt.legend(loc='upper left');
plt.show()


pyplot.show()

